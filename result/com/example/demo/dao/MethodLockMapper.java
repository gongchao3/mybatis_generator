package com.example.demo.dao;

import com.example.demo.model.MethodLock;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author gongchao123
 * @since 2018-12-26
 */
public interface MethodLockMapper extends BaseMapper<MethodLock> {

}
