package com.example.demo.service.impl;

import com.example.demo.model.MethodLock;
import com.example.demo.dao.MethodLockMapper;
import com.example.demo.service.MethodLockService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author gongchao123
 * @since 2018-12-26
 */
@Service
public class MethodLockServiceImpl extends ServiceImpl<MethodLockMapper, MethodLock> implements MethodLockService {

}
