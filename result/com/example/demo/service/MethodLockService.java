package com.example.demo.service;

import com.example.demo.model.MethodLock;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author gongchao123
 * @since 2018-12-26
 */
public interface MethodLockService extends IService<MethodLock> {

}
